var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const line = require('@line/bot-sdk');

const client = new line.Client({
  channelAccessToken : "Zp6dRs80SUqqTvY28Ej9wV7aOg1/sS8ZcDvlgNe7JLffj2VqnT8Tg5EVD9UX7B+F98fybBl8XkJhYzFCW5PPmAgeagO+WaQfH563j4wXtYIJpu9n5ENtvt9AFc7CYu77d59pIR5qo/B0112Ob0bx4gdB04t89/1O/w1cDnyilFU="
});

console.log("debug line client loaded " , client);

var indexRouter = require('./routes/index');
//var usersRouter = require('./routes/users');
var webhookRouter = require('./routes/webhook');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/webhook' , webhookRouter );

app.use('/', indexRouter);
//app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
